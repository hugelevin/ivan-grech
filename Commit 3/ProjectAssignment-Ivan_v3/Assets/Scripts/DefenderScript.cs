﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderScript : MonoBehaviour {

	public int cost = 100;
	private StarDisplay starDisplay;

	void Start() {
		starDisplay = GameObject.FindObjectOfType<StarDisplay> ();
	}

	public void AddStars(int amount) {
		starDisplay.AddToUI (amount);
	}
}

