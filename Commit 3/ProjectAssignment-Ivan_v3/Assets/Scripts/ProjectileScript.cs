﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {

	public float speed, damage;

	void Update () {
		transform.Translate (Vector3.right * speed * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D col) {
		AttackerScript attacker = col.gameObject.GetComponent<AttackerScript> ();
		HealthScript health = col.gameObject.GetComponent<HealthScript> ();
		if (attacker && health) {
			health.DealDamage(damage);
			Destroy(gameObject);
		}
	}
}
