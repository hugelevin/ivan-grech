﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

	ButtonController[] buttonArray;
	public GameObject defenderPrefab;
	public static GameObject selectedDefender;

	void Start() {
		buttonArray = GameObject.FindObjectsOfType<ButtonController> ();
		foreach (ButtonController thisButton in buttonArray) {
			thisButton.GetComponent<SpriteRenderer>().color = Color.gray;
		}
	}

	void OnMouseDown() {

		foreach (ButtonController thisButton in buttonArray) {
			thisButton.GetComponent<SpriteRenderer>().color = Color.gray;
		}
		selectedDefender = defenderPrefab;
		GetComponent<SpriteRenderer>().color = Color.white;
	}
}
