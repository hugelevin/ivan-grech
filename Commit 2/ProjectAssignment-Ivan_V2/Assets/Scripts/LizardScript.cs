﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LizardScript : MonoBehaviour {

	private AttackerScript attacker;
	private Animator anim;

	void Start () {
		attacker = gameObject.GetComponent<AttackerScript> ();
		anim = gameObject.GetComponent<Animator> ();
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (!col.GetComponent<DefenderScript> ()) {
			return;
		}

		anim.SetBool ("IsAttacking", true);
		attacker.Attack(col.gameObject);
	}
}
