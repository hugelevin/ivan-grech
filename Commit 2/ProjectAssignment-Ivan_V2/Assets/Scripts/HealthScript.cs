﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {

	public float hitpoints = 100f;

	public void DealDamage(float damage) {
		hitpoints -= damage;
		if (hitpoints <= 0) {
			Die();
		}
	}

	public void Die() {
		Destroy(gameObject);
	}
}
