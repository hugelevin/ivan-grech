﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public float waitTime;

	void Start () {
		if (waitTime != 0) {
			Invoke ("LoadNextLevel", waitTime);
		} 
	}

	public void QuitGame () {
		Application.Quit ();
	}

	public void LoadNextLevel() 
	{
		CancelInvoke("LoadNextLevel");
		SceneManager.LoadScene (Application.loadedLevel + 1);
	}

	public void LoadLevel(string name) 
	{
		if (name.Equals (null)) {
			Debug.Log ("error");
		} else {
			SceneManager.LoadScene (name);
		}
	}
}
