﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour {
	public float hitpoints = 100f;

	public void DealDamage(float damage) {
		hitpoints -= damage;
		if (hitpoints <= 0) {
			Die();
		}
	}

	public void Die() {
		Destroy(gameObject);
	}
}