﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

	public Slider volumeSlider;
	public Slider difficultySlider;
	public LevelManager levelManager;
	private MusicManager musicManager;

	void Start () {
		musicManager = GameObject.FindObjectOfType <MusicManager>();
	}	

	public void ChangeVolume () {
		musicManager.ChangeVolume (volumeSlider.value);
	}

	public void SetDefaults() {
		volumeSlider.value = 1;
		difficultySlider.value = 2;
	}
		

	public void Apply() {
		levelManager.LoadLevel ("Menu");
	}
}
