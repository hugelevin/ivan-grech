﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

	public AudioClip levelMusic; 
	private AudioSource source;

	void Awake () {
		DontDestroyOnLoad (gameObject);
	}

	void Start() {
		source = GetComponent<AudioSource> ();
		source.clip = levelMusic;
		source.Play ();
		source.loop = true;
		//ChangeVolume (PlayerPrefsManager.GetMasterVolume());
	}

	public void ChangeVolume(float volume) {
		GetComponent<AudioSource> ().volume = volume;
	}
}
